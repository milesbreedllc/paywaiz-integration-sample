#PayWaiz application integration sample#

This is a sample application that shows how you can integrate card payments with PayWaiz into your own app.
In order to use this sample you need a PayWaiz app installed on same phone and you must be a registered
PayWaiz partner. If you already have access to your PayWaiz portal you may grab your PayWaiz app from https://paywaiz.com.ng/portal

If you are not registered yet you may request for partnership at https://paywaiz.com.ng/requestForm.html

## How it works ##

In order to integrate PayWaiz with your application you need to make a few simple steps:

1. Create an intent 
2. Fill parameters like transaction amount or email/sms receipt address
3. Run intent with startActivityForResult()
4. Receive payment result

On the first run you may see a popup window asking you to choose the desired app for the payment action. Once you select PayWaiz there you will not see it again.

## Code sample ##

This code is taken from PaymentDataActivity:

```
#!java
  // IntegrationMain is the entry point of PayWaiz app
        Intent intent = new Intent("com.paywaiz.mpos.IntegrationMain");

        // This is the operator password used to log in the PayWaiz app. It was set on first PayWaiz launch
        intent.putExtra("password", "00000");

        // Fill transaction amount
        intent.putExtra("amount", 200.0);
      
        // Fill receipt data. At least one of 'email' and 'sms' fields must be set
        intent.putExtra("email", "customer@example.com");
        intent.putExtra("sms", "");

        // Invoke PayWaiz activity and wait for result which will be delivered to onActivityResult()
        startActivityForResult(intent, 0);

```
## Intent parameters ##

|Parameter|Type|Description
|---------|----|------------
|password|String|Operator password, which is used to log in to PayWaiz app
|amount|Double|Transaction amount
|email|String|Customer email address that will receive transaction receipt
|sms|String|Customer phone number that will receive transaction receipt sms

At least one of receipt fields (email or sms) must be filled so that customer receives at least one receipt.

## Security ##

PayWaiz app stores all sensitive data in encrypted form. AES with 128 bit key is used, and the key itself is derieved from operator password.
So without operator password no other app can use PayWaiz or get access to its data. This is why it is needed by the integrating app that needs
to use PayWaiz features.
While you may use any password in the real world app, this sample code uses a fixed "00000" operator password just for the sake of easiness of code. Make sure you set it when running PayWaiz for the first time.

## Response codes ##

These are the codes that will be returned to your application onActivityResult() method:

| Response code | Description                                                                                                               |
|---------------|---------------------------------------------------------------------------------------------------------------------------|
| 0             | Payment completed successfully                                                                                            |
| 1             | Operator password not provided                                                                                            |
| 2             | Operator password is wrong                                                                                                |
| 3             | Amount is not provided or is invalid                                                                                      |
| 4             | No receipt address is provided. Either sms or email address must be set                                                   |
| 5             | PayWaiz application is not set up. After installing it you shall run it at least 1 time to perform initial configuration. |
| 6             | Transaction was cancelled by operator  
| 7             | Internal error