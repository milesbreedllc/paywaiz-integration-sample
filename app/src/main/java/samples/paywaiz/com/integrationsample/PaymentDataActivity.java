package samples.paywaiz.com.integrationsample;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class PaymentDataActivity extends AppCompatActivity {

    private EditText amountEdit;

    private EditText emailEdit;

    private EditText smsEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_data);

        amountEdit = (EditText) findViewById(R.id.amountEdit);
        emailEdit = (EditText) findViewById(R.id.emailEdit);
        smsEdit = (EditText) findViewById(R.id.phoneEdit);

    }

    /**
     * Reads values from edit boxes and starts PayWaiz activity
     */
    public void makePayment(View view) {
        // IntegrationMain is the entry point of PayWaiz app
        Intent intent = new Intent("com.paywaiz.mpos.IntegrationMain");

        // This is the operator password used to log in the PayWaiz app. It was set on first PayWaiz launch
        intent.putExtra("password", "00000");

        // Fill transaction amount
        try {
            intent.putExtra("amount", Double.parseDouble(amountEdit.getText().toString()));
        } catch (NumberFormatException ex) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.invalid_amount)
                    .setTitle(R.string.error)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
            ;

            builder.setCancelable(true);
            builder.show();
            return;
        }

        // Fill receipt data. At least one of 'email' and 'sms' fields must be set
        intent.putExtra("email", emailEdit.getText().toString());
        intent.putExtra("sms", smsEdit.getText().toString());

        // Invoke PayWaiz activity and wait for result which will be delivered to onActivityResult()
        startActivityForResult(intent, 0);
    }

    /**
     * Receives results of a payment
     * @param resultCode Code returned by PayWaiz. 0 means transaction was successful
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.transaction_result_title)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
        ;

        if (resultCode == 0) {
            builder.setMessage(getString(R.string.transaction_success));
        } else {
            builder.setMessage(getString(R.string.transaction_result) + " " + resultCode);
        }
        builder.setCancelable(true);
        builder.show();
    }
}
